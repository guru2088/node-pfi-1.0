const mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost:27017/PFI',{ useNewUrlParser: true });

mongoose.connect('mongodb://PFIAdmin:Ft436Lo@18.221.84.179:47923/PFI?authSource=admin',{ useNewUrlParser: true });


var db = mongoose.connection;

db.on('error', function (err) {
console.log('connection error', err);
});

db.once('open', function () {
console.log('connected.');
});

function saveData(obj,coll,callback) {
    obj.save(function (err,res) {
       return callback(err, res)
    });
}

function updateData(obj,conditions,updatequery,callback) {
    obj.updateOne(conditions,updatequery,{upsert:true}, function(err, res) {
      return callback(err, res)
    });
}

function getData(obj,condition,callback) {
    obj.find(condition, function(err, res) {
      return callback(err, res)
    });
}

function getData_paginate(obj,condition,limit,skip,callback) {
    obj.find(condition,null,{limit:limit,skip:skip}, function(err, res) {
      return callback(err, res)
    });
}

function getlast_orderID(obj,callback) {
    obj.findOne({}, {}, { sort: { '_id' : -1 } }, function(err, res) {
        return callback(err, res)
    });
}

function getlast_userID(obj,callback) {
    obj.findOne({}, {}, { sort: { '_id' : -1 } }, function(err, res) {
        return callback(err, res)
    });
}

function dropCollection(obj,callback) {
    obj.collection.drop(function(err, res) {
      return callback(err, res)
    });
}

function findOneAndUpdate(obj,condition,doc,callback) {

    obj.findOneAndUpdate(condition, doc, { upsert: true }, function(err, res) {
      return callback(err, res)
    });
}


function cleanup() {
  mongoose.disconnect();
  return true;
}

module.exports = {
  saveData: saveData,
  updateData: updateData,
  getData:getData,
  dropCollection:dropCollection,
  findOneAndUpdate:findOneAndUpdate,
  getlast_orderID:getlast_orderID,
  getData_paginate:getData_paginate,
  getlast_userID:getlast_userID
};
