const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const rates = new Schema({
  "product":String,
  "rate":Number,
  "type":String,
  "distributorID":Number,
  "restrictType":String,
  modifiedAt:{
    type:Date
  }
});

mongoose.model('rates', rates);
