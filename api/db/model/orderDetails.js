const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderDetails = new Schema({
  orderID:{
    type:Number,
    required: true,
    unique:true
  },
  orderDate: Date,
  mm8: Number,
  mm10:Number,
  mm12:Number,
  mm16:Number,
  mm20:Number,
  mm25:Number,
  mm32:Number,
  bw:Number,
  userID:Number,
  totalPrice:Number,
  status:String,
  paymentDetails:{
    amount:Number,
    utrNo:String
  },
  cgst:Number,
  sgst:Number,
  igst:Number,
  ordertype:String,
  deliveryStateID:Number,
  address:String,
  createdAt:{
    type:Date,
    default: Date.now
  },
  modifiedAt:{
    type:Date
  }
});

mongoose.model('orderDetails', orderDetails);
