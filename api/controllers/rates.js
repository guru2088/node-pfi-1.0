'use strict';
const mongoose = require('mongoose');
var util = require('util');
var userDetails = require('../db/model/userDetails');
var userDetailsSchema = mongoose.model('userDetails');
var rateDetails = require('../db/model/rates');
var rateDetailsSchema = mongoose.model('rates');
var dbcon = require("../db/dbcon");

/* POST */

function saverate(req, res){

  var rateObj = {
    "product":req.swagger.params.product.value,
    "rate":req.swagger.params.rate.value,
    "type":req.swagger.params.type.value,
    "distributorID":req.swagger.params.distributorID.value,
    "restrictType":req.swagger.params.restrictType.value,
    "modifiedAt": new Date()
  };

  var condition = {
    "distributorID":req.swagger.params.distributorID.value,
    "type":req.swagger.params.type.value,
    "product":req.swagger.params.product.value
  };

  dbcon.updateData(rateDetailsSchema,condition,rateObj, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}



function getallrates_admin(req,res){

  var condition = {

  };

  dbcon.getData(rateDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });
}

function getrates(req,res){

  var condition = {
      userID: req.swagger.params.userID.value
  };

  dbcon.getData(userDetailsSchema,condition, function (err, response) {
    if (!err){

      if(response[0]['distributorID'] != ''){
        var condition_rate = {
            distributorID: response[0]['distributorID'] ,
            type:req.swagger.params.type.value
        };

        dbcon.getData(rateDetailsSchema,condition_rate, function (err, response) {
          if (!err){
            res.json(response);
          }
          else{
            res.json(err);
          }
        });
      }
      // res.json(response);
    }
    else{
      res.json(err);
    }
  });

}




function getrates_product(req,res){

  var condition = {
      userID: req.swagger.params.userID.value
  };

  dbcon.getData(userDetailsSchema,condition, function (err, response) {
    if (!err){

      if(response[0]['distributorID'] != ''){
        var condition_rate = {
            distributorID: response[0]['distributorID'] ,
            type:req.swagger.params.type.value,
            product:req.swagger.params.product.value
        };

        dbcon.getData(rateDetailsSchema,condition_rate, function (err, response) {
          if (!err){
            res.json(response);
          }
          else{
            res.json(err);
          }
        });
      }
      // res.json(response);
    }
    else{
      res.json(err);
    }
  });

}


function getallrates(req,res){

  var condition = {
      userID: req.swagger.params.userID.value
  };

  dbcon.getData(userDetailsSchema,condition, function (err, response) {
    if (!err){

      if(response[0]['distributorID'] != ''){
        var condition_rate = {
            distributorID: response[0]['distributorID']
        };

        dbcon.getData(rateDetailsSchema,condition_rate, function (err, response) {
          if (!err){
            res.json(response);
          }
          else{
            res.json(err);
          }
        });
      }
    }
    else{
      res.json(err);
    }
  });

}


module.exports = {
  saverate: saverate,
  getrates: getrates,
  getrates_product:getrates_product,
  getallrates:getallrates,
  getallrates_admin:getallrates_admin
};
