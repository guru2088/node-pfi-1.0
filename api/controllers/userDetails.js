'use strict';
const mongoose = require('mongoose');
var util = require('util');
var userDetails = require('../db/model/userDetails');
var userDetailsSchema = mongoose.model('userDetails');
var dbcon = require("../db/dbcon");

/* GET */
function login(req, res){

  var condition = {
      email: req.swagger.params.email.value,
      saltPassword: req.swagger.params.saltPassword.value
  };
  dbcon.getData(userDetailsSchema,condition, function (err,response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

/* GET */
function getallusers(req, res){

  var condition = {};
  dbcon.getData(userDetailsSchema,condition, function (err,response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}



/* GET */
function getdistributordetails(req, res){


  var condition = {
      userID: req.swagger.params.userID.value
  };
  dbcon.getData(userDetailsSchema,condition, function (err, response) {
    if (!err){
      var condition_distributor = {
          userID: response[0]['distributorID']
      };
      dbcon.getData(userDetailsSchema,condition_distributor, function (err, response) {
        if (!err){
          res.json(response);
        }
        else{
          res.json(err);
        }
      });
    }
    else{
      //console.log(res.json(response[0].distributorID))
      res.json([{"Error":err}]);
    }
  });

}

/* POST */
function register(req, res){

  var email = req.swagger.params.email.value;
  var email_formatted = util.format('%s', email);

  dbcon.getlast_userID(userDetailsSchema, function (err, response) {
    if (!err){

      var userID = 0;
      if(response == null){
        userID = 1;
      }
      else{
        userID = response['userID'] + 1;
      }

      const userDetailsObj_register = new userDetailsSchema({
        userID: userID,
        name: {
          fName:req.swagger.params.fName.value,
          lName:req.swagger.params.lName.value
        },
        email:email_formatted,
        saltPassword:req.swagger.params.saltPassword.value,
        mobile:req.swagger.params.mobile.value,
        firmName:req.swagger.params.firmName.value,
        dateofBirth:new Date(req.swagger.params.dateofBirth.value),
        address1:req.swagger.params.address1.value,
        address2:req.swagger.params.address2.value,
        address3:req.swagger.params.address3.value,
        pincode:req.swagger.params.pincode.value,
        stateID:req.swagger.params.stateID.value,
        gstNo:req.swagger.params.gstNo.value,
        userType:req.swagger.params.userType.value,
        distributorID:req.swagger.params.distributorID.value,
        createdAt: new Date()
      });

      dbcon.saveData(userDetailsObj_register,"userDetails", function (err, response) {
        if (!err){
          res.json([{userID:response.userID,status:"User created successfully"}]);
        }
        else{
          res.json(err);
        }
      });
    }
  });

}

module.exports = {
  getdistributordetails:getdistributordetails,
  login: login,
  register: register,
  getallusers:getallusers
};
