'use strict';
const mongoose = require('mongoose');
var util = require('util');
var rateDetails = require('../db/model/rates');
var rateDetailsSchema = mongoose.model('rates');
var orderDetails = require('../db/model/orderDetails');
var orderDetailsSchema = mongoose.model('orderDetails');
var dbcon = require("../db/dbcon");

var state = require('../constants/constants');


function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;
}

/* POST */
function saveorder(req, res){
  /* Generate order ID */
  var orderID = 0;
  var condition = {};
  dbcon.getlast_orderID(orderDetailsSchema, function (err, response) {
    if (!err){
      if(response == null){
        orderID = 1;
      }
      else{
        orderID = response['orderID'] + 1;
      }

      var date = convertUTCDateToLocalDate(new Date());



      /* Save order */
      if (orderID != 0){
        const orderObj = new orderDetailsSchema({
          orderID: orderID,
          orderDate: date.toLocaleString(),
          mm8:req.swagger.params.mm8.value,
          mm10:req.swagger.params.mm10.value,
          mm12:req.swagger.params.mm12.value,
          mm16:req.swagger.params.mm16.value,
          mm20:req.swagger.params.mm20.value,
          mm25:req.swagger.params.mm25.value,
          mm32:req.swagger.params.mm32.value,
          bw:req.swagger.params.bw.value,
          userID:req.swagger.params.userID.value,
          totalPrice:req.swagger.params.totalPrice.value,
          status:"Order Placed",
          paymentDetails:{
            amount:req.swagger.params.paymentDetailsamount.value,
            utrNo:req.swagger.params.utrNo.value
          },
          cgst:req.swagger.params.cgst.value,
          sgst:req.swagger.params.sgst.value,
          igst:req.swagger.params.igst.value,
          ordertype:req.swagger.params.ordertype.value,
          deliveryStateID:req.swagger.params.deliveryStateID.value,
          address:req.swagger.params.address.value,
          createdAt:date.toLocaleString()
        });

        dbcon.saveData(orderObj,"orderDetails", function (err, response) {
          if (!err){
            res.json([{orderID:response.orderID,status:"Order created successfully"}]);
          }
          else{
            console.log("err --" + err);
            res.json(err);
          }
        });
      }
    }
    else{
      res.json(err);
    }
  });

}

/* POST */
function updateUTR(req, res){

  var conditions = { orderID: req.swagger.params.orderID.value, userID : req.swagger.params.userID.value };

  var updatequery = {
    paymentDetails: {
      utrNo:req.swagger.params.utrNo.value
    },
    status: "UTR updated",
    updateDate: new Date()
  };

  dbcon.updateData(orderDetailsSchema,conditions,updatequery, function (err, response) {
    if (!err){
      res.json([{status:"UTR updated successfully"}]);
    }
    else{
      res.json([{status:"UTR not updated"}]);
    }
  });

}

/* GET */
function getOrder_orderID(req, res){
  var condition = {orderID: req.swagger.params.orderID.value};
  dbcon.getData(orderDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });
}

/* GET */
function getOrder_userID(req, res){
  var condition = {userID: req.swagger.params.userID.value};
  dbcon.getData(orderDetailsSchema,condition, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}


/* GET */
function getallOrders(req, res){
  var condition = {};
  var limit = req.swagger.params.limit.value;
  var skip =  req.swagger.params.skip.value;
  dbcon.getData_paginate(orderDetailsSchema,condition,limit,skip, function (err, response) {
    if (!err){
      res.json(response);
    }
    else{
      res.json(err);
    }
  });

}

module.exports = {
  saveorder: saveorder,
  updateUTR: updateUTR,
  getOrder_orderID:getOrder_orderID,
  getOrder_userID:getOrder_userID,
  getallOrders:getallOrders
};
